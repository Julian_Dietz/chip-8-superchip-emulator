### An emulator for Chip-8 and Superchip games, ROMs included!  

The controls are based on the original controller and made for QWERTZ keyboards.
Input keys are:

1234  
qwer  
asdf  
yxcv  

The games differ greatly from another, so there will be some input fiddling required :)

### Building the project  

In order to compile the project, you need:

* Visual Studio (I've used VS13) 
* Get GLFW, available here: http://www.glfw.org/download.html 
* Fix include paths for your GLFW dlls 
