#pragma once
#include "glad/include/glad/glad.h"
#include "glfw3.h"

#include "Structures.h"

class Glfw_window
{
public: 	
	void EmulatorMain();
	class Chip8* GetChip8() const;
	void ToggleSwapInterval();
	Glfw_window();
	~Glfw_window();

private:
	void Init();
	void CleanUp();

	GLFWwindow* Window;
	const GLuint SCALE;
	const GLuint WIDTH;
	const GLuint HEIGHT;

	GLuint ShaderProgram;
	bool UseDebugSwapInterval;

	class Chip8* Emulator;
	const char* DefaultGamePath;
};

