#pragma once
#include <iostream>

#include "Chip8.h"
#include "Glfw_window.h"
#include "Shaders.h"

void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mode);
void DropCallback(GLFWwindow* window, int count, const char** paths);

Glfw_window::Glfw_window() :
	Window(nullptr),
	SCALE(16),
	WIDTH(64 * SCALE),
	HEIGHT(32 * SCALE),
	ShaderProgram(0),
	UseDebugSwapInterval(false),
	Emulator(nullptr),
	DefaultGamePath("Chip8Pack/SuperChip Games/Joust [Erin S. Catto, 1993].ch8")
{
	Init();
}

Glfw_window::~Glfw_window()
{
	CleanUp();
}

void Glfw_window::Init()
{
	Emulator = new Chip8();
	Emulator->Init();
	Emulator->LoadGame(DefaultGamePath);

	// Init GLFW
	glfwInit();
	// Set all the required options for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	// Create a GLFWwindow object that we can use for GLFW's functions
	Window = glfwCreateWindow(WIDTH, HEIGHT, "Chip 8", NULL, NULL);
	glfwMakeContextCurrent(Window);
	if (Window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return;
	}

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize OpenGL context" << std::endl;
		return;
	}

	// Define the viewport dimensions
	glViewport(0, 0, WIDTH, HEIGHT);

	// Create Vertex Array Object
	GLuint Vao;
	glGenVertexArrays(1, &Vao);
	glBindVertexArray(Vao);

	// Create a Vertex Buffer Object and copy the vertex data to it
	GLuint Vbo;
	glGenBuffers(1, &Vbo);

	GLfloat Vertices[] = 
	{
		//  Position   Texcoords
		-1.0f,  1.0f, 0.0f, 0.0f, // Top-left
		1.0f,  1.0f, 1.0f, 0.0f, // Top-right
		1.0f, -1.0f,  1.0f, 1.0f, // Bottom-right
		-1.0f, -1.0f,  0.0f, 1.0f  // Bottom-left
	};

	glBindBuffer(GL_ARRAY_BUFFER, Vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);

	// Create an element array
	GLuint Ebo;
	glGenBuffers(1, &Ebo);

	GLuint Elements[] = {
		0, 1, 2,
		2, 3, 0
	};

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Elements), Elements, GL_STATIC_DRAW);

	// Create and compile the vertex shader
	GLuint VertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(VertexShader, 1, &VertexShaderSource, NULL);
	glCompileShader(VertexShader);

	// Create and compile the fragment shader
	GLuint FragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(FragmentShader, 1, &FragmentShaderSource, NULL);
	glCompileShader(FragmentShader);

	// Link the vertex and fragment shader into a shader program
	ShaderProgram = glCreateProgram();
	glAttachShader(ShaderProgram, VertexShader);
	glAttachShader(ShaderProgram, FragmentShader);
	glBindFragDataLocation(ShaderProgram, 0, "outColor");
	glLinkProgram(ShaderProgram);
	glUseProgram(ShaderProgram);

	// Specify the layout of the vertex data
	GLint PosAttrib = glGetAttribLocation(ShaderProgram, "position");
	glEnableVertexAttribArray(PosAttrib);
	glVertexAttribPointer(PosAttrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);


	GLint TexAttrib = glGetAttribLocation(ShaderProgram, "texcoord");
	glEnableVertexAttribArray(TexAttrib);
	glVertexAttribPointer(TexAttrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (void*)(2 * sizeof(GLfloat)));

	//Create Texture
	GLuint Tex;
	glGenTextures(1, &Tex);
	glBindTexture(GL_TEXTURE_2D, Tex);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, Emulator->GetWidth(), Emulator->GetHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, Emulator->GetPixels());

	glUniform1i(glGetUniformLocation(ShaderProgram, "screenTex"), 0);

	glfwSwapInterval(1);

	glfwSetWindowUserPointer(Window, this);
	glfwSetDropCallback(Window, DropCallback);
	glfwSetKeyCallback(Window, KeyCallback);
}

void Glfw_window::EmulatorMain()
{
	while (!glfwWindowShouldClose(Window))
	{
		glfwPollEvents();

		Emulator->Emulate();
		if (Emulator->EndEmulation())
		{
			glfwSetWindowShouldClose(Window, GL_TRUE);
		}

		// Render
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, Emulator->GetWidth(), Emulator->GetHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, Emulator->GetPixels());
		glUniform1i(glGetUniformLocation(ShaderProgram, "screenTex"), 0);

		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glfwSwapBuffers(Window);
	}
}


// Is called whenever a key is pressed/released via GLFW
void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	Glfw_window *MyWindow = reinterpret_cast<Glfw_window *>(glfwGetWindowUserPointer(window));
	
	if ((key == GLFW_KEY_I) && action == GLFW_PRESS)
	{
		MyWindow->ToggleSwapInterval();
	}

	MyWindow->GetChip8()->SetKeyPressed(window, key, scancode, action, mode);
}


void DropCallback(GLFWwindow* window, int count, const char** paths)
{
	std::string FilePath(*paths);
	Glfw_window* MyWindow = reinterpret_cast<Glfw_window *>(glfwGetWindowUserPointer(window));

	MyWindow->GetChip8()->Init();
	MyWindow->GetChip8()->LoadGame(FilePath);
}

Chip8* Glfw_window::GetChip8() const
{ 
	return Emulator; 
}

void Glfw_window::ToggleSwapInterval()
{
	UseDebugSwapInterval = !UseDebugSwapInterval;
	glfwSwapInterval(UseDebugSwapInterval ? 0 : 1);
}

void Glfw_window::CleanUp()
{
	glfwTerminate();
	if (Emulator != nullptr)
	{
		delete Emulator;
		Emulator = nullptr;
	}
}