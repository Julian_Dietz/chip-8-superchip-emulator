#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

#include "Chip8.h"
#include "glfw3.h"
#include "RomChecksums.h"

struct ScreenDimensions
{
	static const u16 WidthChip8 = 64;
	static const u16 HeightChip8 = 32;
	static const u16 NumPxChip8 = WidthChip8 * HeightChip8;

	static const u16 WidthSuper = 128;
	static const u16 HeightSuper = 64;
	static const u16 NumPxSuper = WidthSuper * HeightSuper;
};

//Init can also be used as reset
void Chip8::Init() 
{
	RegPC = 0x200;					// Programs start at 0x200 for chip8
	CurrentOpcode = 0;
	RegI = 0;
	StackIndex = 0;
	std::rand();std::rand();std::rand();
	DelayTimer = 0;
	SoundTimer = 0;
	bEndEmulation = false;

	Pixels.resize(ScreenDimensions::NumPxSuper);
	ClearPixels();

	const size_t SizeRegs = sizeof(Reg) / sizeof(Reg[0]);;
	for (size_t i = 0; i < SizeRegs; ++i)
	{
		Reg[i] = 0;
		Stack[i] = 0;
		PressedKeys[i] = false;
	}

	const size_t SizeUserFlags = sizeof(RPLUserFlags) / sizeof(RPLUserFlags[0]);
	for (size_t i = 0; i < SizeUserFlags; ++i)
	{
		RPLUserFlags[i] = 0;
	}

	const size_t SizeFontChip8 = size_t(sizeof(Chip8Font) / sizeof(Chip8Font[0]));
	const size_t SizeFontSuper = size_t(sizeof(SuperFont) / sizeof(SuperFont[0]));
	memcpy(Memory, Chip8Font, SizeFontChip8);
	memcpy(Memory + SizeFontChip8, SuperFont, SizeFontSuper);

	const size_t SizeRAM = size_t(sizeof(Memory) / sizeof(Memory[0]));
	for (size_t i = SizeFontChip8 + SizeFontSuper; i < SizeRAM; ++i)
	{
		Memory[i] = 0;
	}

	CompabilityMode = ECompabilityMode::Default;
	SetEmulatorMode(EEmulatorMode::Chip8);
	ResetDebugFile();
}

const Pixel* Chip8::GetPixels() const
{
	return Pixels.data();
}

int Chip8::GetWidth() const
{
	return GetEmulatorMode() == EEmulatorMode::Chip8 ? ScreenDimensions::WidthChip8 : ScreenDimensions::WidthSuper;

}

int Chip8::GetHeight() const
{
	return GetEmulatorMode() == EEmulatorMode::Chip8 ? ScreenDimensions::HeightChip8 : ScreenDimensions::HeightSuper;
}

//Read a rom and store it in memory
void Chip8::LoadGame(const std::string &fileName) {
	std::ifstream file(fileName, std::ifstream::binary);
	assert(file.is_open());

	file.seekg(0, std::ios::end);
	const int Count = (int)file.tellg();
	file.seekg(0, std::ios::beg);
	file.read((char*) (Memory + 0x200), Count);

	const int RomChecksum = Count * Memory[0x20A];

	CompabilityMode = RomChecksum == CHECKSUM_ANIMALRACE ? ECompabilityMode::AnimalRace :
						RomChecksum == CHECKSUM_VERS ? ECompabilityMode::VERS : ECompabilityMode::Default;
}

void Chip8::Emulate() {
	if (RegPC + 1 < sizeof(Memory) / sizeof(u8))
	{
		CurrentOpcode = Memory[RegPC] << 8 | Memory[RegPC + 1];							
		RegPC += 2;
		
		std::cout << RegPC - 0x02 << " " << std::hex << CurrentOpcode  << std::endl;	// Print it
		//WriteDebug(CurrentOpcode, RegPC, RegI, Stack, Reg);
		
		switch (CurrentOpcode & 0xF000) 
		{
		case 0x0000:	
			if ((CurrentOpcode & 0x00F0) == 0x00C0) //[SUPERCHIP]: Scroll down texture by 0x00C[N] bits 
			{
				assert(EmulatorMode == EEmulatorMode::SuperChip);
				const size_t NumLines = (CurrentOpcode & 0x000F);
				const size_t Width = GetWidth();
				const size_t Height = GetHeight();

				for (size_t Y = 0; Y < Height - NumLines; ++Y)
				{
					for (size_t X = 0; X < Width; ++X)
					{
						const size_t CurrentPixelIndex = (Height - 1 - NumLines - Y) * Width + X;
						const size_t TargetPixelIndex = CurrentPixelIndex + NumLines * Width;
						Pixels[TargetPixelIndex] = Pixels[CurrentPixelIndex];
					}
				}
				for (size_t Index = 0; Index < NumLines * Width; ++Index)
				{
					Pixels[Index] = Pixel::Black;
				}
				break;
			}
			
			switch (CurrentOpcode & 0x00FF)
			{
			case 0x00E0:	// 00E0: Clear the screen
			{
				ClearPixels();
				break;
			}
			case 0x00EE:	// 00EE: return from a subroutine
			{
				--StackIndex;
				RegPC = Stack[StackIndex];
				break;
			}
			case 0x00FB:	// [SUPERCHIP8]: 00FB: Scroll display 4 pixels to the right
			{
				const size_t Width = (size_t)(GetWidth());
				const size_t Height = (size_t)(GetHeight());

				for (size_t Y = 0; Y < Height; ++Y)
				{
					for (size_t X = 0; X < Width - 4; ++X)
					{
						const size_t TargetPixelIndex = Y * Width + Width - X - 1;
						const size_t CurrentPixelIndex = TargetPixelIndex - 4;
						Pixels[TargetPixelIndex] = Pixels[CurrentPixelIndex];
					}
					for (size_t X = 0; X < 4; ++X)
					{
						Pixels[Y * Width + X] = Pixel::Black;
					}
				}
				break;
			}
			case 0x00FC:	// [SUPERCHIP8]: 00FC: Scroll display 4 pixels to the left
			{
				const size_t Width = (size_t)(GetWidth());
				const size_t Height = (size_t)(GetHeight());

				for (size_t Y = 0; Y < Height; ++Y) 
				{
					for (size_t X = 0; X < Width - 4; ++X)
					{
						const size_t TargetPixelIndex = Y * Width + X;
						const size_t CurrentPixelIndex = Y * Width + X + 4;
						Pixels[TargetPixelIndex] = Pixels[CurrentPixelIndex];
					}
					for (size_t X = 0; X < 4; ++X)
					{
						Pixels[Y * Width + Width - X - 1] = Pixel::Black;
					}
				}
				break;
			}
			case 0x00FD:	// [SUPERCHIP8]: End Emulation
			{
				bEndEmulation = true;
				break;
			}
			case 0x00FE:	// [SUPERCHIP8]: 00FE: Disable extended screen mode for full-screen graphics (128 x 64)
			{
				SetEmulatorMode(EEmulatorMode::Chip8);
				ClearPixels();
				break;
			}
			case 0x00FF:	// [SUPERCHIP8]: 00FF: Enable extended screen mode for full-screen graphics (128 x 64)
			{
				SetEmulatorMode(EEmulatorMode::SuperChip);
				ClearPixels();
				break;
			}
			default:
				InvalidCode();
				break;
			}
			break;
		case 0x1000:	// 1NNN: Jumps to adress NNN
		{
			RegPC = CurrentOpcode & 0x0FFF;
			break;
		}
		case 0x2000:	// 2NNN: Calls a subroutine at NNN
		{
			Stack[StackIndex] = RegPC;
			++StackIndex;
			RegPC = CurrentOpcode & 0x0FFF;
			break;
		}
		case 0x3000:	// 3XNN: skips next instruction if VX == NN
		{
			if (Reg[(CurrentOpcode & 0x0F00) >> 8] == (CurrentOpcode & 0x00FF))
			{
				RegPC += 2;
			}
			break;
		}
		case 0x4000:	// 4XNN: Skips the next instruction if VX doesnt equal NN
		{
			if (Reg[(CurrentOpcode & 0x0F00) >> 8] != (CurrentOpcode & 0x00FF)) 
			{
				RegPC += 2;
			}
			break;
		}
		case 0x5000:	// 5XY0: Skips next instruction if VX equals VY
		{
			if (Reg[(CurrentOpcode & 0x0F00) >> 8] == Reg[(CurrentOpcode & 0x00F0) >> 4])
			{
				RegPC += 2;
			}
			break;
		}
		case 0x6000:	// 6XNN: SETS VX to NN
		{
			Reg[(CurrentOpcode & 0x0F00) >> 8] = (CurrentOpcode & 0x00FF);
			break;
		}
		case 0x7000:	// 7XNN, adds NN to VX
		{
			Reg[(CurrentOpcode & 0x0F00) >> 8] += (CurrentOpcode & 0x00FF);
			break;
		}
		case 0x8000:
		{
			switch (CurrentOpcode & 0x000F) {
			case 0x0000:	// 8XY0: Sets VX to THE VALUE of VY
				Reg[(CurrentOpcode & 0x0F00) >> 8] = Reg[(CurrentOpcode & 0x00F0) >> 4];
				break;
			case 0x0001:	// 8XY1 Sets VX to VX or VY;
				Reg[(CurrentOpcode & 0x0F00) >> 8] |= Reg[(CurrentOpcode & 0x00F0) >> 4];
				break;
			case 0x0002:	// 8XY2: Sets VX to VX and VY. 
				Reg[(CurrentOpcode & 0x0F00) >> 8] = Reg[(CurrentOpcode & 0x0F00) >> 8] & Reg[(CurrentOpcode & 0x00F0) >> 4];
				break;
			case 0x0003:	// 8XY3: Sets VX to VX xor VY
				Reg[(CurrentOpcode & 0x0F00) >> 8] = Reg[(CurrentOpcode & 0x0F00) >> 8] ^ Reg[(CurrentOpcode & 0x00F0) >> 4];
				break;
			case 0x0004:	// 8XY4: Adds VY to VX. VF is set to 1 when theres a carry and 0 when there isnt
			{
				if ((Reg[(CurrentOpcode & 0x00F0) >> 4] + Reg[(CurrentOpcode & 0x0F00) >> 8]) > 255)
				{
					Reg[0xF] = 1;
				}
				else
				{
					Reg[0xF] = 0;
				}
				Reg[(CurrentOpcode & 0x0F00) >> 8] += Reg[(CurrentOpcode & 0x00F0) >> 4];
				break;
			}
			case 0x0005:	// 8XY5: VX - VY. if borrowFlag, then VF = 0; else VF = 1
				if (Reg[(CurrentOpcode & 0x0F00) >> 8] < Reg[(CurrentOpcode & 0x00F0) >> 4]) // borrowflag is needed
				{
					Reg[0xF] = 0;
				}
				else
				{
					Reg[0xF] = 1;
				}

				Reg[(CurrentOpcode & 0x0F00) >> 8] -= Reg[((CurrentOpcode & 0x00F0) >> 4)];
				break;
			case 0x0006:	// 8XY6: Shifts VX right by one.
							//VF is set to the value of the least significant bit of VX before the shift
				Reg[0xF] = Reg[(CurrentOpcode & 0x0F00) >> 8] & 1;
				Reg[(CurrentOpcode & 0x0F00) >> 8] = Reg[(CurrentOpcode & 0x0F00) >> 8] >> 1;
				break;
			case 0x0007:	// 8XY7: sets VX to VY minus VX. if borrow, VF = 0, else VF = 1
				if (Reg[(CurrentOpcode & 0x0F00) >> 8] > Reg[(CurrentOpcode & 0x00F0) >> 4])
				{
					Reg[0xF] = 0;
				}
				else
				{
					Reg[0xF] = 1;
				}
				Reg[(CurrentOpcode & 0x0F00) >> 8] = Reg[(CurrentOpcode & 0x00F0) >> 4] - Reg[(CurrentOpcode & 0x0F00) >> 8];
				break;
			case 0x000E: // 8X0E: store bit 7 in VF, shift VX left by 1
				Reg[0xF] = Reg[(CurrentOpcode & 0x0F00) >> 8] >> 7;
				Reg[(CurrentOpcode & 0x0F00) >> 8] = Reg[(CurrentOpcode & 0x0F00) >> 8] << 1;
				break;
			default:
				InvalidCode();
				break;
			}
			break;
		}
		case 0x9000:	 // 9XY0 : Skips the next instruction if VX doesn't equal VY.
			if (Reg[(CurrentOpcode & 0x0F00) >> 8] != Reg[(CurrentOpcode & 0x00F0) >> 4]) 
			{
				RegPC += 2;
			}
			break;
		case 0xA000:	 // ANNN: Set RegI to NNNN
			RegI = CurrentOpcode & 0x0FFF;
			break;
		case 0xB000:	 // BNNN: Jump to address NNN + V[0]
			RegPC = (CurrentOpcode & 0x0FFF) + Reg[0];
			break;
		case 0xC000:	 // CXNN: VX = random number AND NN 
		{
			Reg[(CurrentOpcode & 0x0F00) >> 8] = rand() & (CurrentOpcode & 0x00FF);
			break;
		}

		case 0xD000:	 //DXYN: Draw sprite at screen location(register VX, register VY) height N
		{
			const int X = Reg[(CurrentOpcode & 0x0F00) >> 8];
			const int Y = Reg[((CurrentOpcode & 0x00F0) >> 4)];
			const int SpriteHeight = (CurrentOpcode & 0x000F) == 0 ? 16 : (CurrentOpcode & 0x000F);
			const int SpriteWidth = (SpriteHeight == 16) ? 16 : 8;
			const int ScreenWidth = GetWidth();
			const int ScreenHeight = GetHeight();

			Reg[0xF] = 0; // CollisionFlag

			for (int CurrentRow = 0; CurrentRow < SpriteHeight; ++CurrentRow)
			{
				for (int CurrentColumn = 0; CurrentColumn < SpriteWidth; ++CurrentColumn)
				{
					const int RowCount = CurrentColumn / 8;
					const int BitShift = CurrentColumn % 8;
					bool bDraw = false;

					if (SpriteHeight == 16)
					{
						bDraw = ((Memory[RegI + (CurrentRow * 2) + RowCount] & (0x80 >> BitShift)) != 0);
					}
					else
					{
						bDraw = ((Memory[RegI + CurrentRow + RowCount] & (0x80 >> BitShift)) != 0);
					}

					if (bDraw)
					{
						int PixelIndex = X + (Y * ScreenWidth) + (CurrentRow * ScreenWidth) + CurrentColumn;

						if (CompabilityMode == ECompabilityMode::VERS)
						{
							PixelIndex %= ScreenWidth * ScreenHeight;
						}

						if ((size_t)(PixelIndex) < Pixels.size())
						{
							if (Pixels[PixelIndex]())
							{
								Reg[0xF] = 1;
								Pixels[PixelIndex] = Pixel::Black;
							}
							else 
							{
								Pixels[PixelIndex] = Pixel::White;
							}
						}
					}
				}
			}
		}
		break;

		case 0xE000:
			switch (CurrentOpcode & 0x00FF) {
			case 0x009E:	// EX9E: Skips the next instruction if the key stored in VX is pressed
							// HEX input: 0-F
				for (int i = 0; i < 16; ++i)
				{
					if (PressedKeys[i])
					{
						if (i == Reg[((CurrentOpcode & 0x0F00) >> 8)])
							RegPC += 2;
					}
				}
				break;
			case 0x00A1:	//EXA1 - Skips the next instruction if the key stored in VX ISNT pressed
			{
				bool isKeyPressed = false;

				for (int i = 0; i < 16; ++i)
				{
					if (PressedKeys[i])
					{
						if (i == Reg[((CurrentOpcode & 0x0F00) >> 8)])
						{
							isKeyPressed = true;
						}
					}
				}

				if (!isKeyPressed)
				{
					RegPC += 2;
				}
			}
				break;
			default:
				InvalidCode();
				break;
			}
			break;

		case 0xF000:
			switch (CurrentOpcode & 0x00FF) 
			{
			case 0x0007:	// FX07 Sets VX to the value of the delay timer
				Reg[(CurrentOpcode & 0x0F00) >> 8] = DelayTimer;
				break;
			case 0x000A:	// FX0A: A key press is awaited and stored in VX
			{
				bool KeyPressed = false;
				for (int i = 0; i < 16; ++i)
				{
					if (PressedKeys[i])
					{
						Reg[(CurrentOpcode & 0x0F00) >> 8] = i;
						KeyPressed = true;
						break;
					}
				}
				if (!KeyPressed)
					RegPC -= 2;

				break;
			}
			case 0x0015:	// FX15: Sets the delay timer to VX
			{
				DelayTimer = Reg[(CurrentOpcode & 0x0F00) >> 8];
				break;
			}
			case 0x0018:	//Sets the sound timer to VX.
			{
				SoundTimer = Reg[(CurrentOpcode & 0x0F00) >> 8];
				break;
			}
			case 0x001E:	//Adds VX to I.
				/*	
				if (RegI + Reg[(CurrentOpcode & 0x0F00) >> 8] > 0xFFF)
					Reg[0xF] = 1;
				else
					Reg[0xF] = 0;
				*/
				RegI += Reg[(CurrentOpcode & 0x0F00) >> 8];
				/*if (RegI > 0xFFF) RegI -= 0xFFF;*/
				break;
			case 0x0029:	
							// FX29: Sets I to the location of the sprite for the character 
							// in VX. Characters are Represented by a 4x5 font
				RegI = Reg[(CurrentOpcode & 0x0F00) >> 8] * 5;
				break;
			case 0x0030:	//[SUPERCHIP]: FX30: Make RegI point to the sprite stored in VX 
				RegI = 80 + Reg[(CurrentOpcode & 0x0F00) >> 8] * 10;
				break;
			case 0x0033:	//FX33 : store the value of VX as decimal value. store 100 digit at RegI, 10 digit at RegI+1, 1 digit at RegI + 2
			{
				int decNum = Reg[(CurrentOpcode & 0x0F00) >> 8];
				Memory[RegI] = (decNum % 1000) / 100;
				Memory[RegI + 1] = (decNum % 100) / 10;
				Memory[RegI + 2] = (decNum % 10);
				break;
			}
			case 0x0055:	// FX55: Store registers V0-VX at RegI in memory
				for (int i = 0; i <= ((CurrentOpcode & 0x0F00) >> 8); ++i) 
				{
					Memory[RegI + i] = Reg[i];
				}
				if (CompabilityMode == ECompabilityMode::AnimalRace)
				{
					RegI += (Reg[(CurrentOpcode & 0x0F00) >> 8] + 1);
				}
				break;
			case 0x0065:	// FX65: load registers V0-VX from RegI in memory
				for (int i = 0; i <= ((CurrentOpcode & 0x0F00) >> 8) ; ++i) 
				{
					Reg[i] = Memory[RegI + i];
				}
				if (CompabilityMode == ECompabilityMode::AnimalRace)
				{
					RegI += (Reg[(CurrentOpcode & 0x0F00) >> 8] + 1);
				}
				break;
			case 0x0075:	//[SUPERCHIP]: FX75 Store V0..VX in RPL user flags (X <= 7)
				for (int i = 0; i < ((CurrentOpcode & 0x0F00) >> 8); ++i) 
				{
					RPLUserFlags[i] = Reg[i];
				}
				break;
			case 0x0085:	//[SUPERCHIP]: FX85 Read V0..VX from RPL user flags (X <= 7) 
				for (int i = 0; i < ((CurrentOpcode & 0x0F00) >> 8); ++i)
				{
					Reg[i] = RPLUserFlags[i];
				}
				break;
			default:
				InvalidCode();
				break;
			}
			break;
		default:
			InvalidCode();
			break;
		}	
	}

	if (DelayTimer > 0)
	{
		DelayTimer--;
	}

	if (SoundTimer > 0) 
	{
		SoundTimer--;
		if (SoundTimer == 0)
		{
			//TODO: Find a more pleasing sound than the bell
			// Disabled this annoying torture for know

			//std::cout << (char)(7) << std::endl;
		}
	}
}

//Translate the keypress logic to an Chip8 Layout
void Chip8::SetKeyPressed(GLFWwindow* window, int key, int scancode, int action, int mods) 
{
	const bool InputSet = action == GLFW_PRESS || action == GLFW_REPEAT;
	
	switch (key) {
	case GLFW_KEY_1:
		PressedKeys[1] = InputSet;
		break;
	case GLFW_KEY_2:
		PressedKeys[2] = InputSet;
		break;
	case GLFW_KEY_3:
		PressedKeys[3] = InputSet;
		break;
	case GLFW_KEY_4:
		PressedKeys[12] = InputSet;
		break;
	case GLFW_KEY_Q:
		PressedKeys[4] = InputSet;
		break;
	case GLFW_KEY_W:
		PressedKeys[5] = InputSet;
		break;
	case GLFW_KEY_E:
		PressedKeys[6] = InputSet;
		break;
	case GLFW_KEY_R:
		PressedKeys[13] = InputSet;
		break;
	case GLFW_KEY_A:
		PressedKeys[7] = InputSet;
		break;
	case GLFW_KEY_S:
		PressedKeys[8] = InputSet;
		break;
	case GLFW_KEY_D:
		PressedKeys[9] = InputSet;
		break;
	case GLFW_KEY_F:
		PressedKeys[14] = InputSet;
		break;
	case GLFW_KEY_Y:
		PressedKeys[10] = InputSet;
		break;
	case GLFW_KEY_Z:	// Keyboard Layout
		PressedKeys[10] = InputSet;
		break;
	case GLFW_KEY_X:
		PressedKeys[0] = InputSet;
		break;
	case GLFW_KEY_C:
		PressedKeys[11] = InputSet;
		break;
	case GLFW_KEY_V:
		PressedKeys[15] = InputSet;
		break;
	default:
		break;
	}
}

const EEmulatorMode Chip8::GetEmulatorMode() const
{
	return EmulatorMode;
}

const bool Chip8::EndEmulation() const
{
	return bEndEmulation;
}

void Chip8::SetEmulatorMode(EEmulatorMode NewMode)
{
	EmulatorMode = NewMode;
}

void Chip8::ClearPixels()
{
	for (size_t i = 0; i < Pixels.size(); ++i)
	{
		Pixels[i] = Pixel::Black;
	}
}

void Chip8::InvalidCode() const
{
	std::cout << "Invalid code. Check: " << RegPC << " " << std::hex << CurrentOpcode << std::endl;
}

//Writes all relevant data to a file called "DebugLog.txt"
void Chip8::WriteDebug(const u16 CurrentOpcode, const u16 RegPC, const u16 RegI, const u16 *Stack, const u8 *Reg) const
{
	std::ofstream Log("DebugLog.txt", std::ofstream::out | std::ofstream::app);

	Log << "CurrentOpcode: \t0x" << std::hex << CurrentOpcode << std::endl;
	Log << "RegPC: \t\t\t" << std::hex << RegPC << std::endl;
	Log << "RegI: \t\t\t" << std::hex << RegI << std::endl;
	Log << "Stack: \t\t\t";

	for (int i = 0; i < 16; ++i)
	{
		Log << std::hex << Stack[i] << " ";
	}

	Log << std::endl << "Reg: \t\t\t";

	for (int i = 0; i < 16; ++i)
	{
		Log << std::hex << ((Reg[i] == 0) ? 0 : Reg[i]) << " ";
	}

	Log << std::endl;
	Log << "PressedKeys: ";

	//log << "PressedKey: \t" << (int)pressedKey << std::endl;
	for (int i = 0; i < 16; ++i)
	{
		Log << (PressedKeys[i]) << " ";
	}

	Log << std::endl << std::endl << std::endl;
	Log.close();
}

void Chip8::ResetDebugFile() const
{
	std::ofstream log("DebugLog.txt", std::ofstream::trunc);
	log.close();
}
