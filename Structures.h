#pragma once
#include "TypeDefs.h"

struct Pixel
{
	Pixel() :
		R(0), 
		G(0), 
		B(0), 
		A(255)
	{
	}
	
	Pixel(u8 InR, u8 InG, u8 InB, u8 InA) :
		R(InR),
		G(InG),
		B(InB),
		A(InA)
	{
	}

	bool operator()()
	{
		return((R > 0) && (G > 0) && (B > 0));
	}
	
	u8 R;
	u8 G;
	u8 B;
	u8 A;

	static const Pixel Black;
	static const Pixel White;
};

