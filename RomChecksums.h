#pragma once
/*
Checksums are computed like:
int checkSum = (int)file.tellg() * Memory[0x20A]
*/

const int CHECKSUM_ANIMALRACE = 42984;
const int CHECKSUM_VERS = 7360;
